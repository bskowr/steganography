from PIL import Image


class SteganographyPhotos:
    """
    Module for encrypting images in other images. The functions assume both cover and hidden photos are same size.
    """

    @staticmethod
    def tuple_int_to_bin(pixel_data):
        """Converts RGB tuple from int to binary string
        (255, 255, 000) -> ('11111111', '11111111', '00000000')

        :param pixel_data: Tuple of three 8-bit integers (corresponding to RGB values)
        :return: Tuple of three 8-bit binary strings (corresponding to RGB values)
        """
        r, g, b = pixel_data
        return (f'{r:08b}',
                f'{g:08b}',
                f'{b:08b}')

    @staticmethod
    def tuple_bin_to_int(pixel_data):
        """
        Converts RGB tuple from binary string to int
        ('11111111', '11111111', '00000000') -> (255, 255, 000)

        :param pixel_data: Tuple of three 8-bit binary strings (corresponding to RGB values)
        :return: Tuple of three 8-bit integers (corresponding to RGB values)
        """
        r, g, b = pixel_data
        return (int(r, 2),
                int(g, 2),
                int(b, 2))

    @staticmethod
    def calculate_bits(ratio, max_bits=8):
        """
        Calculates amount of bits assigned to cover and hidden, based on ratio
        Example:
            ratio = (12, 4), max_bits=8
            cover_bits = 6 <- 8/(12+4) * 12 rounded to nearest integer
            hidden_bits = 2 <- 8/(12+4) * 4 rounded to nearest integer
        :param ratio: Tuple of two integers (x, y) corresponding to ratio x:y
        :param max_bits: max amount of bits (default 8)
        :return: Tuple of two integers, corresponding to amount of bits assigned to cover and hidden photo
        """
        shares_sum = sum(ratio)
        cover_bits = round(max_bits / shares_sum * ratio[0])
        hidden_bits = round(max_bits / shares_sum * ratio[1])
        return cover_bits, hidden_bits

    @staticmethod
    def merge_rgb(cover_pixel, hidden_pixel, cover_bits, hidden_bits):
        """
        Merges RGB values from cover photo pixel and hidden photo pixel, based on assigned bits.
        Values must be in binary string format (convert integers using tuple_int_to_bin function)
        Example:
            cover_pixel = ('11110010', '10010010', '00000011'), cover_bits = 6
            hidden_pixel = ('00101111', '11010110', '01110000'), hidden_bits = 2
            final_pixel = ('11111111', '10010110', '00000000')
        :param cover_pixel: Tuple of three 8-bit binary strings making up a pixel from cover photo
        :param hidden_pixel: Tuple of three 8-bit binary strings making up a pixel from hidden photo
        :param cover_bits: bits assigned to cover photo
        :param hidden_bits: bits assigned to hidden photo
        :return: Tuple of three 8-bit binary string making up a new pixel with encrypted data
        """
        cover_r, cover_g, cover_b = cover_pixel
        hidden_r, hidden_g, hidden_b = hidden_pixel
        return (cover_r[:cover_bits] + hidden_r[:hidden_bits],
                cover_g[:cover_bits] + hidden_g[:hidden_bits],
                cover_b[:cover_bits] + hidden_b[:hidden_bits])

    @staticmethod
    def unmerge_rgb(encrypted_pixel, cover_bits, hidden_bits, max_bits=8):
        """
        Gets RGB values of cover photo and hidden photo from encrypted pixel, based on bits assigned to each.
        Adds padding zeros to the end of extracted data, to ensure it has eight bits
        Values must be in binary string format (convert integers using tuple_int_to_bin function)
        Example:
            encrypted_pixel = ('11110010', '10010010', '00000011'), cover_bits = 6, hidden_bits = 2, max_bits=8
            cover_pixel = ('11110000', '10010000', '00000000')
            hidden_pixel = ('10000000', '10000000', '11000000')
        :param encrypted_pixel: Tuple of three 8-bit binary strings making up a pixel from encrypted photo
        :param cover_bits: bits assigned to cover photo
        :param hidden_bits: bits assigned to hidden photo
        :param max_bits: maximum amount of bits in return data (default 8)
        :return: Tuple of two tuples, corresponding to pixel in cover photo and hidden photo accordingly
        """
        encrypted_r, encrypted_g, encrypted_b = encrypted_pixel
        padding_cover = max_bits - cover_bits
        padding_hidden = max_bits - hidden_bits
        cover = (encrypted_r[:cover_bits] + '0' * padding_cover,
                 encrypted_g[:cover_bits] + '0' * padding_cover,
                 encrypted_b[:cover_bits] + '0' * padding_cover,)
        hidden = (encrypted_r[cover_bits:] + '0' * padding_hidden,
                  encrypted_g[cover_bits:] + '0' * padding_hidden,
                  encrypted_b[cover_bits:] + '0' * padding_hidden,)
        return (cover, hidden)

    @staticmethod
    def merge_images(cover_photo, hidden_photo, ratio):
        """
        Encrypts hidden_photo in cover_photo with given ratio of cover_bits to hidden_bits
        :param cover_photo: Image object of photo that will be visible after encryption
        :param hidden_photo: Image object of photo that will be hidden after encryption
        :param ratio: Tuple of two numbers, corresponding to ratio of cover_bits to hidden_bits. Example: ratio=16,4 -> cover_bits=6, hidden_bits=2. Best to use ratios summing up to 8
        :return: Returns cover image with hidden image at last bits. Last bits depend on passed ratio. Using too many bits for hidden image may decrease quality of cover photo, and vice versa
        """
        cover_bits, hidden_bits = SteganographyPhotos.calculate_bits(ratio)
        pixel_map_cover = cover_photo.load()
        pixel_map_hidden = hidden_photo.load()
        new = Image.new(cover_photo.mode, cover_photo.size)
        pixel_map_new = new.load()
        for i in range(cover_photo.size[0]):
            for j in range(cover_photo.size[1]):
                cover_pixel = SteganographyPhotos.tuple_int_to_bin(pixel_map_cover[i, j])
                hidden_pixel = SteganographyPhotos.tuple_int_to_bin((0, 0, 0))
                if i < hidden_photo.size[0] or j < hidden_photo.size[1]:
                    hidden_pixel = SteganographyPhotos.tuple_int_to_bin(pixel_map_hidden[i, j])
                new_pixel = SteganographyPhotos.merge_rgb(cover_pixel, hidden_pixel, cover_bits, hidden_bits)
                SteganographyPhotos.tuple_bin_to_int(new_pixel)
                pixel_map_new[i, j] = SteganographyPhotos.tuple_bin_to_int(new_pixel)
        return new

    @staticmethod
    def unmerge_images(encrypted_image, ratio):
        """
        Splits encrypted photo into two photos: cover and hidden. Bits taken for cover and hidden depend on ratio. Best to use ratio summing up to 8
        :param encrypted_image: Image object of photo with another photo encrypted in it
        :param ratio: Tuple of two numbers, corresponding to ratio of cover_bits to hidden_bits. Example: ratio=16,4 -> cover_bits=6, hidden_bits=2. Best to use ratios summing up to 8
        :return: Tuple of two Image objects - (cover, hidden). First one is the "visible" one, the other was hidden in visible one. Quality of both depends on ratio.
        """
        cover_bits, hidden_bits = SteganographyPhotos.calculate_bits(ratio)
        pixel_map_encrypted = encrypted_image.load()
        decrypted_cover = Image.new(encrypted_image.mode, encrypted_image.size)
        pixel_map_decrypted_cover = decrypted_cover.load()
        decrypted_hidden = Image.new(encrypted_image.mode, encrypted_image.size)
        pixel_map_decrypted_hidden = decrypted_hidden.load()
        for i in range(encrypted_image.size[0]):
            for j in range(encrypted_image.size[1]):
                encrypted_pixel = SteganographyPhotos.tuple_int_to_bin(pixel_map_encrypted[i, j])
                cover_pixel, hidden_pixel = SteganographyPhotos.unmerge_rgb(encrypted_pixel, cover_bits, hidden_bits,
                                                                            cover_bits + hidden_bits)
                pixel_map_decrypted_cover[i, j] = SteganographyPhotos.tuple_bin_to_int(cover_pixel)
                pixel_map_decrypted_hidden[i, j] = SteganographyPhotos.tuple_bin_to_int(hidden_pixel)
        return decrypted_cover, decrypted_hidden
